import 'package:flutter/material.dart';

showSimpleAlert(BuildContext context,{String message}) {
  showDialog(context: context,
  builder: (_){
    return AlertDialog(
      content: Text(message),
      actions: [
        FlatButton(onPressed: () => Navigator.pop(context), child: Text("ok"))
      ],
    );
  });
}