import 'dart:ui' as ui;
import 'dart:typed_data';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:image/image.dart' as img;
import 'package:path/path.dart' as _path;
import 'package:path_provider/path_provider.dart';
import 'dart:io';

void logError(String code, String message) {
  if (message != null) {
    print('Error: $code\nError Message: $message');
  } else {
    print('Error: $code');
  }
}

Future<Uint8List> getBytesImgFromAsset(String path) async {
  ByteData data = await rootBundle.load(path);
  ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List());
  ui.FrameInfo fi = await codec.getNextFrame();
  return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
      .buffer
      .asUint8List();
}

Future<String> mergeImage(File _file, String fileName) async {
  File imageFile;
  File croppedFile;

  File newFile = new File(_file.absolute.path);

  ImageProperties originalPrp =
      await FlutterNativeImage.getImageProperties(newFile.absolute.path);

  imageFile = await FlutterNativeImage.compressImage(newFile.absolute.path,
      quality: 100, targetWidth: 1240, targetHeight: 2204
      /*(originalPrp.height * 1240 / originalPrp.width).round()*/);

  ImageProperties properties =
      await FlutterNativeImage.getImageProperties(imageFile.absolute.path);

  double _originY = 180;
  //(properties.height - 1844) / 2;

  croppedFile = await FlutterNativeImage.cropImage(
      imageFile.absolute.path, 0, _originY.toInt(), 1240, 1844);

  final borderBytes =
      await getBytesImgFromAsset('assets/images/cadre_portrait.png');
  final image1 = img.decodeImage(borderBytes);

  //final image2 = img.decodeImage(File(newFile.absolute.path).readAsBytesSync());
  final image2 =
      img.decodeImage(File(croppedFile.absolute.path).readAsBytesSync());

  final mergedImage = img.Image(1240, 1844);

  img.copyInto(mergedImage, image2, blend: false);
  img.copyInto(mergedImage, image1);

  final documentDirectory = await getApplicationDocumentsDirectory();

  final file =
      File(_path.join(documentDirectory.path, "${fileName}_merged_image.png"));

  file.writeAsBytesSync(img.encodePng(mergedImage));

  return file.absolute.path;
}
