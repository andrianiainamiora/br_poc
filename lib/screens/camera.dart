import 'dart:async';
import 'dart:io';

import 'package:br_poc/screens/preview.dart';
import 'package:br_poc/tools/dialogs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:image/image.dart' as img;
import 'dart:math';
import 'package:path/path.dart' as _path;

class CameraScreen extends StatefulWidget {
  @override
  _CameraScreenState createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {
  CameraController cameraController;
  List cameras;
  int selectedCameraIndex;
  String imgPath;
  bool boolVal;

  Future initCamera(CameraDescription cameraDescription) async {
    if (cameraController != null) {
      await cameraController.dispose();
    }

    cameraController =
        CameraController(cameraDescription, ResolutionPreset.ultraHigh);

    cameraController.addListener(() {
      if (mounted) {
        setState(() {});
      }
    });

    if (cameraController.value.hasError) {
      print('Camera Error ${cameraController.value.errorDescription}');
    }

    try {
      await cameraController.initialize();
    } catch (e) {
      showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  /// Display camera preview

  Widget cameraPreview() {
    if (cameraController == null || !cameraController.value.isInitialized) {
      return Text(
        'Chargement',
        style: TextStyle(
            color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),
      );
    }

    //final size = MediaQuery.of(context).size;
    //final deviceRatio = size.width / size.height;

    final size = MediaQuery.of(context).size.width;

    return AspectRatio(
      aspectRatio: 2.0 / 3.0, //cameraController.value.aspectRatio,
      //child: CameraPreview(cameraController),
      child: Transform.scale(
        scale: 1.0, //cameraController.value.aspectRatio / deviceRatio,
        child: Center(
          child: AspectRatio(
            aspectRatio: 2.0 / 3.0, //cameraController.value.aspectRatio,
            child: OverflowBox(
              alignment: Alignment.center,
              child: FittedBox(
                fit: BoxFit.fitWidth,
                child: Container(
                  width: size,
                  height: size / cameraController.value.aspectRatio,
                  child: Stack(
                    children: <Widget>[
                      CameraPreview(cameraController),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget cameraControl(context) {
    return Expanded(
      child: Align(
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            FloatingActionButton(
              child: Icon(
                Icons.camera,
                color: Colors.black,
              ),
              backgroundColor: Colors.white,
              onPressed: () {
                //onCapture(context);
                onTakePictureButtonPressed();
              },
            )
          ],
        ),
      ),
    );
  }

  Widget cameraToggle() {
    if (cameras == null || cameras.isEmpty) {
      return Spacer();
    }

    CameraDescription selectedCamera = cameras[selectedCameraIndex];
    CameraLensDirection lensDirection = selectedCamera.lensDirection;

    return Expanded(
      child: Align(
        alignment: Alignment.centerLeft,
        child: FlatButton.icon(
            onPressed: () {
              onSwitchCamera();
            },
            icon: Icon(
              getCameraLensIcons(lensDirection),
              color: Colors.white,
              size: 24,
            ),
            label: Text(
              '${lensDirection.toString().substring(lensDirection.toString().indexOf('.') + 1).toUpperCase()}',
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
            )),
      ),
    );
  }

  Future<String> takePicture() async {
    if (!cameraController.value.isInitialized) {
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${DateTime.now()}.jpg';

    if (cameraController.value.isTakingPicture) {
      return null;
    }

    try {
      await cameraController.takePicture(filePath);
    } on CameraException catch (e) {
      return null;
    }
    return filePath;
  }

  void onTakePictureButtonPressed() async {
    File imageFile;
    File croppedFile;
    final name = DateTime.now();

    takePicture().then((String filePath) async {
      if (mounted) {
        imgPath = filePath;

        /* ImageProperties originalPrp =
            await FlutterNativeImage.getImageProperties(imgPath);

        imageFile = await FlutterNativeImage.compressImage(imgPath,
            quality: 100, targetWidth: 1240, targetHeight: 2204
            /*(originalPrp.height * 1240 / originalPrp.width).round()*/);

        ImageProperties properties =
            await FlutterNativeImage.getImageProperties(
                imageFile.absolute.path);

        double _originY = 180;
        //(properties.height - 1844) / 2;

        croppedFile = await FlutterNativeImage.cropImage(
            imageFile.absolute.path, 0, _originY.toInt(), 1240, 1844);*/

        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PreviewScreen(
                      imgPath: imgPath, //croppedFile.absolute.path,
                      fileName: "$name.png",
                      fileImage: new File(imgPath),
                    )));

        setState(() {});
      }
    });
  }

  onCapture(context) async {
    File imageFile;
    File croppedFile;

    try {
      //final p = await getTemporaryDirectory();
      final name = DateTime.now();
      //final path = "${p.path}/$name.png";

      final path = _path.join(
        (await getTemporaryDirectory()).path,
        '${DateTime.now()}.png',
      );

      await cameraController.takePicture(path);

      ImageProperties originalPrp =
          await FlutterNativeImage.getImageProperties(path);

      String val1 = originalPrp.height.toString();
      String val2 = originalPrp.width.toString();

      showSimpleAlert(context, message: "H>" + val1 + "W>" + val2);

      imageFile = await FlutterNativeImage.compressImage(path,
          quality: 100,
          targetWidth: 1240,
          targetHeight:
              (originalPrp.height * 1240 / originalPrp.width).round());

      ImageProperties properties =
          await FlutterNativeImage.getImageProperties(imageFile.absolute.path);

      double _originY = (properties.height - 1844) / 2;

      croppedFile = await FlutterNativeImage.cropImage(
          imageFile.absolute.path, 0, _originY.toInt(), 1240, 1844);

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PreviewScreen(
                    imgPath: null, //croppedFile.absolute.path,
                    fileName: "$name.png",
                    fileImage: croppedFile,
                  )));

      /*await cameraController.takePicture(path).then((value) async {
        print('here');
        print(path);

        ImageProperties originalPrp =
            await FlutterNativeImage.getImageProperties(path);

        imageFile = await FlutterNativeImage.compressImage(path,
            quality: 100,
            targetWidth: 1240,
            targetHeight:
                (originalPrp.height * 1240 / originalPrp.width).round());

        ImageProperties properties =
            await FlutterNativeImage.getImageProperties(
                imageFile.absolute.path);

        double _originY = (properties.height - 1844) / 2;

        croppedFile = await FlutterNativeImage.cropImage(
            imageFile.absolute.path, 0, _originY.toInt(), 1240, 1844);

        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PreviewScreen(
                      imgPath: croppedFile.absolute.path,
                      fileName: "$name.png",
                      fileImage: croppedFile,
                    )));
      });*/
    } catch (e) {
      showCameraException(e);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    availableCameras().then((value) {
      cameras = value;
      if (cameras.length > 0) {
        setState(() {
          selectedCameraIndex = 0;
        });
        initCamera(cameras[selectedCameraIndex]).then((value) {});
      } else {
        print('No camera available');
      }
    }).catchError((e) {
      print('Error : ${e.code}');
    });
  }

  double getScreenHeight(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return height;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.redAccent,
      body: Container(
        child: Stack(
          children: <Widget>[
//            Expanded(
//              flex: 1,
//              child: _cameraPreviewWidget(),
//            ),
            Align(
              alignment: Alignment.center,
              child: cameraPreview(),
            ),
            Align(
              alignment: Alignment.center,
              child: new AspectRatio(
                aspectRatio: 2 / 3,
                child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/cadre_portrait.png"),
                          fit: BoxFit.fill)),
                ),
              ),
            ),
            /*Align(
              // A Enlevé ... Pour Test
              alignment: Alignment.topCenter,
              child: imgPath == null
                  ? null
                  : SizedBox(
                      child: Image.file(File(imgPath)),
                      height: 290.0,
                    ),
            ),*/
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 120,
                width: double.infinity,
                padding: EdgeInsets.all(15),
                color: Colors.transparent,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    cameraToggle(),
                    cameraControl(context),
                    Spacer(),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  getCameraLensIcons(lensDirection) {
    switch (lensDirection) {
      case CameraLensDirection.back:
        return CupertinoIcons.switch_camera;
      case CameraLensDirection.front:
        return CupertinoIcons.switch_camera_solid;
      case CameraLensDirection.external:
        return CupertinoIcons.photo_camera;
      default:
        return Icons.device_unknown;
    }
  }

  onSwitchCamera() {
    selectedCameraIndex =
        selectedCameraIndex < cameras.length - 1 ? selectedCameraIndex + 1 : 0;
    CameraDescription selectedCamera = cameras[selectedCameraIndex];
    initCamera(selectedCamera);
  }

  showCameraException(e) {
    String errorText = 'BRANDEET Error message: ${e.description}';
    print(errorText);
  }
}
