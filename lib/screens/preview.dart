import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:br_poc/tools/tools.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:printing/printing.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:image/image.dart' as img;

class PreviewScreen extends StatefulWidget {
  final String imgPath;
  String fileName;
  File fileImage;

  PreviewScreen({this.imgPath, this.fileName, this.fileImage});

  @override
  _PreviewScreenState createState() => _PreviewScreenState();
}

class _PreviewScreenState extends State<PreviewScreen> {
  File imageFile;
  String mergefilePath;

  get list => null;

  @override
  void initState() {
    super.initState();

    mergefilePath = null;
    // if (widget.fileImage != null) {
    //   callMergeImage();
    // }
  }

  Future<String> callMergeImage() async {
    mergefilePath = await mergeImage(widget.fileImage, widget.fileName);

    return mergefilePath;
  }

  printImage() async {
    final pdf = pw.Document();

    final image = pw.MemoryImage(
      File(mergefilePath).readAsBytesSync(),
    );

    pdf.addPage(pw.Page(
        pageFormat: PdfPageFormat.a5,
        build: (pw.Context context) {
          return pw.AspectRatio(
            aspectRatio: 2 / 3,
            child: pw.Image(image),
          ); // Center
        })); // Page

    await Printing.layoutPdf(
        onLayout: (PdfPageFormat format) async => pdf.save());
  }

  _cropImage(filePath) async {
    ImageProperties properties =
        await FlutterNativeImage.getImageProperties(widget.imgPath);
    File croppedFile = await FlutterNativeImage.compressImage(widget.imgPath,
        quality: 100, targetWidth: 1240, targetHeight: 2204);

    // Image thumbnail = copyResize(image, width: 120);
    //File croppedFile =
    //    await FlutterNativeImage.cropImage(widget.imgPath, 0, 0, 720, 960);

    /*File croppedImage = await ImageCropper.cropImage(
      sourcePath: filePath,
      aspectRatioPresets: [CropAspectRatioPreset.ratio3x2],
      maxWidth: 1240,
      maxHeight: 1844,
    );*/

    if (croppedFile != null) {
      imageFile = croppedFile;
    }

    //setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    //_cropImage(widget.imgPath);

    //_cropImage(null);
    //imageFile = new File(widget.imgPath);

    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
        ),
        floatingActionButton: AnimatedOpacity(
          child: FloatingActionButton(
            child: Icon(Icons.print),
            tooltip: "Imprimer",
            backgroundColor: Colors.red,
            onPressed: () {
              printImage();
            },
          ),
          duration: Duration(milliseconds: 100),
          opacity: 1, //Set 0 to hide floatingActionButton
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                new AspectRatio(
                  aspectRatio: 2 / 3,
                  child: Stack(
                    children: <Widget>[
                      /*(mergefilePath == null)
                        ? Container(
                            color: Colors
                                .red) //Image.asset('assets/images/img_test.png')
                        : */
                      FutureBuilder(
                        future: callMergeImage(),
                        initialData: "",
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return circulaar(
                                context); //CircularProgressIndicator();
                          } else if (snapshot.hasData) {
                            return Image.file(
                              File(mergefilePath),
                              fit: BoxFit.fitWidth,
                            );
                          } else {
                            return circulaar(
                                context); //CircularProgressIndicator();
                          }
                        },
                      ),
                      Image.asset('assets/images/cadre_portrait.png'),
                    ],
                  ),
                ),
                /*Expanded(
                flex: 2,
                child: Image.file(
                  //_cropImage(widget.imgPath),
                  File(widget.imgPath),
                  fit: BoxFit.cover,
                ),
              ),*/
                /*Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: double.infinity,
                  height: 32,
                  color: Colors.blueGrey,
                  child: Center(
                    child: IconButton(
                      icon: Icon(
                        Icons.print,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        printImage();
                        /*getBytes().then((bytes) {
                          print('here now');
                          print(widget.imgPath);
                          print(bytes.buffer.asUint8List());
                          Share.file('Share via', widget.fileName,
                              bytes.buffer.asUint8List(), 'image/path');
                        });*/
                      },
                    ),
                  ),
                ),
              )*/
              ],
            ),
          ),
        ));
  }

  Widget circulaar(context) {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Future getBytes() async {
    Uint8List bytes = File(mergefilePath).readAsBytesSync() as Uint8List;
//    print(ByteData.view(buffer))
    return ByteData.view(bytes.buffer);
  }
}
